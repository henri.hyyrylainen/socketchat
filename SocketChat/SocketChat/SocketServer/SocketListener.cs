﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    public class SocketListener
    {
        // Incoming data from the client.  
        public static string data = null;

        public static List<Socket> _listClients = new List<Socket>();
        public static List<Clienthandler> clienthandlers = new List<Clienthandler>();
                
        public static void StartListening()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.  
            // Dns.GetHostName returns the name of the   
            // host running the application.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.  
            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and   
            // listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.  
                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.  
                    Socket handler = listener.Accept();
                    
                    Clienthandler clienthandler = new Clienthandler();
                    clienthandler.StartClientThread(handler);                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        public static void ResponseReceived(string data, Socket socket)
        {
            Console.WriteLine("CLIENT: {0} ", data);
            BroadcastMessage(data);
        }

        public static void BroadcastMessage(string message)
        {
            // Tänne jotain logiikkaa ...
            foreach(Socket client in _listClients)
            {
                byte[] msg = Encoding.ASCII.GetBytes(message);
                client.Send(msg);
            }
        }
    }
}
