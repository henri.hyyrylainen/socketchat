﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketServer
{
    public class Clienthandler
    {
        private Socket _socket;
        private Thread thread;
        
        private string currentMessage = string.Empty;

        public string chatName = string.Empty;
        public void StartClientThread(Socket client)
        {
            _socket = client;
            thread = new Thread(DoTheJob);
            thread.Start();
        }

        private void DoTheJob()
        {
            try
            {
                byte[] buffer = new byte[4096];
                while (true)
                {
                    int len = _socket.Receive(buffer);
                    string data = Encoding.UTF8.GetString(buffer, 0, len);
                    
                    Console.WriteLine(data);

                    if(IsLogin(data))
                    {
                        string message = string.Empty;

                        if (IsNameTakenOrInvalid(chatName))
                        {
                            chatName = string.Empty;
                            message = "LOGIN_FAILED<|>Name already taken";
                        }
                        else
                        {
                            message = $"LOGIN_OK<|> OK";
                            SocketListener._listClients.Add(_socket);
                            SocketListener.clienthandlers.Add(this);
                            SocketListener.BroadcastMessage($"{chatName} just joined...");
                        }

                        byte[] msg = Encoding.ASCII.GetBytes(message);
                        _socket.Send(msg);
                    }
                    else
                    {
                        if(IsAllowedToPostShitInThisChat(chatName))
                        {
                            SocketListener.ResponseReceived($"{chatName} : {currentMessage}", _socket);
                        }                        
                    }
                }
            }catch(SocketException se)
            {
                SocketListener._listClients.Remove(_socket);
                SocketListener.clienthandlers.Remove(this);
                SocketListener.BroadcastMessage($"{chatName} left the chat...");
                Console.WriteLine(se.Message);
                thread.Abort();                
            }
        }

        private bool IsLogin(string data)
        {
            bool ret = false;

            string[] spearator = new string[] { "<|>" };
            string[] dataSplit = data.Split(spearator, StringSplitOptions.None);

            switch (dataSplit[0])
            {
                case "LOGIN":
                    chatName = dataSplit[1];
                    ret = true;
                    break;
                case "MESSAGE":
                    currentMessage = dataSplit[1];
                    ret = false;
                    break;
            }

            return ret;
        }

        private bool IsNameTakenOrInvalid(string name)
        {
            bool ret = false;
            if (String.IsNullOrEmpty(name))
            {
                ret = true;
            }
            else
            {
                foreach (Clienthandler handler in SocketListener.clienthandlers)
                {
                    if (name.Equals(handler.chatName))
                    {
                        ret = true;
                    }
                }
            }

            return ret;
        }

        private bool IsAllowedToPostShitInThisChat(string chatName)
        {
            bool ret = false;

            foreach (Clienthandler handler in SocketListener.clienthandlers)
            {
                if (chatName.Equals(handler.chatName))
                {
                    ret = true;
                }
            }

            return ret;
        }
    }
}
