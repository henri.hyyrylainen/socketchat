﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketChat
{
    public class ResponseEventArgs : EventArgs
    {
        public string ResponseEventString { get; set; }

        public ResponseEventArgs(string myString)
        {
            this.ResponseEventString = myString;
        }
    }
}
