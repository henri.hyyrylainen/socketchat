﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketChat
{
    public class SocketClient
    {
        private static Socket sender = null;
        private static STATUS _status = STATUS.DISCONNECT;
        private static bool stop = false;

        // Data buffer for incoming data.  
        private static byte[] bytes = new byte[1024];

        public static void StartClient()
        {
            stop = false;
            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

                // Create a TCP/IP  socket.  
                sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);
                                        
                    Status = STATUS.WAIT_FOR_LOGIN;
                    string msg = $"Socket connected to {sender.RemoteEndPoint.ToString()}";
                    Console.WriteLine(msg);
                    Program.MainForm.LogMsg(msg);                    
                }
                catch (ArgumentNullException ane)
                {
                    string msg = $"ArgumentNullException : {ane.ToString()}";
                    Console.WriteLine(msg);
                    Program.MainForm.LogMsg(msg);
                }
                catch (SocketException se)
                {
                    string msg = $"SocketException : {se.ToString()}";
                    Console.WriteLine(msg);
                    Program.MainForm.LogMsg(msg);
                }
                catch (Exception e)
                {
                    string msg = $"Unexpected exception :  {e.ToString()}";
                    Console.WriteLine(msg);
                    Program.MainForm.LogMsg(msg);
                }

            }
            catch (Exception e)
            {
                string msg = e.ToString();
                Console.WriteLine(msg);
                Program.MainForm.LogMsg(msg);
            }
        }

        public static void SendMsgServer(string text)
        {
            // Encode the data string into a byte array.  
            byte[] msg = Encoding.ASCII.GetBytes(text);

            // Send the data through the socket.  
            int bytesSent = sender.Send(msg);
        }

        public static void ListenResponse()
        {
            while (!stop)
            {
                try
                {
                    // Receive the response from the remote device.  
                    int bytesRec = sender.Receive(bytes);
                    string data = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    bool success;
                    string msg;

                    if(isLoginResponse(data, out success, out msg))
                    {
                        if(success)
                        {
                            OnResponseReceived(msg);
                            Status = STATUS.CONNECT;
                        }
                        else
                        {
                            OnResponseReceived(msg);
                            Status = STATUS.WAIT_FOR_LOGIN;
                        }
                    }
                    else
                    {
                        OnResponseReceived(msg);
                    }                    
                }
                catch (SocketException se)
                {

                }
            }
        }

        public static void DisconnectClient()
        {
            if (Status != STATUS.DISCONNECT)
            {
                stop = true;
                string msg = "Socket session ended";
                Console.WriteLine(msg);
                Program.MainForm.LogMsg(msg);

                // Release the socket.  
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();

                Status = STATUS.DISCONNECT;
            }
        }

        private static bool isLoginResponse(string response, out bool success, out string msg)
        {
            bool ret = false;
            success = false;
            msg = response;
            string[] spearator = new string[] { "<|>" };
            string[] dataSplit = response.Split(spearator, StringSplitOptions.None);

            switch (dataSplit[0])
            {
                case "LOGIN_FAILED":
                    ret = true;
                    success = false;
                    msg = dataSplit[1];
                    break;
                case "LOGIN_OK":
                    ret = true;
                    success = true;
                    msg = dataSplit[1];
                    break;
            }
            
            return ret;
        }

        public static STATUS Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnStatusChanged();
            }
        }        

        public static event EventHandler StatusChanged;
        public static event EventHandler<ResponseEventArgs> ResponseReceived;
                
        protected static void OnStatusChanged()
        {
            if (StatusChanged != null) StatusChanged(typeof(SocketClient), EventArgs.Empty);
        }
        
        protected static void OnResponseReceived(string data)
        {
            if (ResponseReceived != null) ResponseReceived(typeof(SocketClient), new ResponseEventArgs(data));
        }
    }

    public enum STATUS
    {
        DISCONNECT,
        WAIT_FOR_LOGIN,
        CONNECT
    }
}
