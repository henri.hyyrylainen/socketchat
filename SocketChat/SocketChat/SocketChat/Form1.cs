﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketChat
{
    public partial class Form1 : Form
    {        
        public Form1()
        {
            InitializeComponent();            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SocketClient.StatusChanged += OnStatusChanged;
            SocketClient.ResponseReceived += OnResponseReceived;
            SocketClient.StartClient();
            backgroundWorker.RunWorkerAsync();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SocketClient.DisconnectClient();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            switch(SocketClient.Status)
            {
                case STATUS.DISCONNECT:                    
                    SocketClient.StartClient();
                    backgroundWorker.RunWorkerAsync();
                    break;
                case STATUS.WAIT_FOR_LOGIN:
                    if(!string.IsNullOrWhiteSpace(txtBoxName.Text))
                    {
                        SocketClient.SendMsgServer("LOGIN<|>" + txtBoxName.Text);
                    }
                    break;
                case STATUS.CONNECT:
                    backgroundWorker.Dispose();
                    SocketClient.DisconnectClient();
                    break;
            }
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            switch (SocketClient.Status)
            {
                case STATUS.CONNECT:
                    SocketClient.SendMsgServer("MESSAGE<|>" + txtBoxMessage.Text);
                    txtBoxMessage.Text = string.Empty;
                    break;
                default:
                    MessageBox.Show("Login first, dummy.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    LogMsg("Login first!");
                    break;
            }            
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {            
            SocketClient.ListenResponse();
        }

        private void OnStatusChanged(object sender, EventArgs e)
        {
            switch (SocketClient.Status)
            {
                case STATUS.DISCONNECT:
                    ChangeBtnTxtCrossThread(btnLogin, "Connect");
                    //btnLogin.Text = "Connect";
                    break;
                case STATUS.CONNECT:
                    ChangeBtnTxtCrossThread(btnLogin, "Disconnect");
                    //btnLogin.Text = "Disconnect";
                    break;
                case STATUS.WAIT_FOR_LOGIN:
                    ChangeBtnTxtCrossThread(btnLogin, "Login");
                    //btnLogin.Text = "Login";
                    break;
            }
        }

        public void LogMsg(string text)
        {
            try
            {
                txtBoxLog.AppendText(text + Environment.NewLine);
            }
            catch(ObjectDisposedException ode)
            {

            }            
        }

        private void OnResponseReceived(object sender, ResponseEventArgs e)
        {
            string data = e.ResponseEventString;
            backgroundWorker.ReportProgress(0, data);
        }

        private void BackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string data = e.UserState.ToString();
            LogMsg(data);
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //SocketClient.DisconnectClient();
        }

        private void ChangeBtnTxtCrossThread(Button btn, string text)
        {
            if (btnLogin.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => btn.Text = text));
            }                
            else
            {
                btn.Text = text;
            }               
        }
    }    
}
