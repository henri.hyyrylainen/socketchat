﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketLibrary
{
    public class ChatSocket : Socket
    {
        public string ChatName { get; set; }

        public ChatSocket(SocketInformation socketInformation) : base(socketInformation)
        {            
        }

        public ChatSocket(SocketType socketType, ProtocolType protocolType) : base(socketType, protocolType)
        {
        }

        public ChatSocket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType) : base(addressFamily, socketType, protocolType)
        {
        }
    }
}
